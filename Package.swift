// swift-tools-version: 5.9.2
import PackageDescription

let package = Package(
    name: "TruliooDocV",
    platforms: [
        .iOS(.v14)
    ],
    products: [
        .library(
            name: "TruliooDocV",
            targets: ["TruliooDocV", "TruliooCore", "TruliooSDK"]),
    ],
    dependencies: [
        .package(url: "https://github.com/airbnb/lottie-ios.git", exact: "4.3.3"),
    ],
    targets: [
        .target(
            name: "TruliooDocV"
        ),
        .binaryTarget(
            name: "TruliooCore",
            url: "https://superal-sdk.verification.trulioo.io/2.9.0/TruliooCore.zip",
            checksum: "91ced3ae8c5575cee89d4d775aa42f75476fc8fbd50ce9b337ada0282a803a7e"
        ),
        .binaryTarget(
            name: "TruliooSDK",
            url: "https://superal-sdk.verification.trulioo.io/2.9.0/TruliooSDK.zip",
            checksum: "51991ab7e83b8388633bf389640aa0015b152432af9d7cdfa17dcfeb80bda36a"
        )
    ]
)
