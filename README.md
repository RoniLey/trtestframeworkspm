---
slug: /ios/index.html
---

import Text from '@site/../shared/text.tsx';
import LanguageSupportTable from '@site/../shared/LanguageSupportTable.tsx';

# Trulioo iOS SDK

<Text.BioPolicySDK />

## Overview

The Trulioo iOS SDK facilitates the capture of government-issued identity documents and biometrics for the purpose of identity verification.

Trulioo's sophisticated technology delivers a smooth experience for your end-users, while optimizing for fast and accurate verification.

- The intuitive UI/UX guides your users through the entire photo capture process
- The modular app design allow you to seamlessly connect the image capture process into your own application flow
- Advanced image quality detection leads to higher verification success rates
- Direct image upload to the Trulioo service simplifies integration

> Note: The SDK is only responsible for capturing and uploading photos. Access the Trulioo Customer API for authorization and to perform checks.

------


## Compatibility

To use the DocV SDK with the Customer API, please align the versions according to the below compatibility chart.

| DocV SDK Version | Customer API Version |
|:-----------------|:---------------------|
| 2.6.0 and above  | 2.4.0 and above      |
| 2.0.0 - 2.5.0    | 2.0.0 - 2.4.0        |

------


## Getting started

The Trulioo iOS SDK supports:
- iPhones (excluding iPhone SE) running iOS 14+
- Xcode 12+
    - Integration with any project that uses SwiftUI
    - Integration with any project that uses UIKit (Storyboards)
- Bitcode
- ARM64, ARM64-Simulator, & X86-Simulator
- Portrait screen orientation
- Light mode appearance
- RFC 5646 languages tags

------


### 1. Obtain a transaction shortcode

In order to start your integrating, you will need an API token and REST API integration outside of the application. You will generate a **Shortcode** that lasts up to 5 minutes and provide the Shortcode to your application when ready to launch your Document Verification workflow.

> Note: You must never use API tokens in the frontend of your application or malicious users could discover them in your source code. You should only use them on your server.

------


[//]: # (TODO: revisit once we have cocoapods available)
### 2. Download and install the Trulioo SDK

The Trulioo SDK is available on Cocoapods.

```ruby
pod 'TruliooDocV'
```

### Lottie Animations

If you are using Trulioo DocV ```2.4.0``` or greater, also add this **required** block at the end of your podfile to ensure compatibility.

```ruby
post_install do |installer| 
   installer.pods_project.targets.each do |target| 
      if target.name == 'lottie-ios' 
         target.build_configurations.each do |config|
            config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'
            config.build_settings['EXCLUDED_ARCHS[sdk=iphonesimulator*]'] = 'x86_64'
         end 
      end 
   end
end
```
------


### 3. Grant permissions

The SDK requires camera permissions. Ensure the following keys are included in your apps `info.plist`

```xml
<key>NSCameraUsageDescription</key>
<string>To validate your identity, camera access is required.</string>
```

------


### 4. Create the SDK delegate

Once the SDK has been added as a dependency, you can use it in your projects as a library. Import the `TruliooSDK` library to your ViewController class file, extend your ViewController class with the `TruliooDelegate` and add the required delegate functions as follows:

```swift
import UIKit
import TruliooSDK

class YourViewController : UIViewController, TruliooDelegate {
    func onInitialized() {
        print("SDK Initialized")
    }
    
    func onComplete(result: TruliooSuccess) {
        print("Verification success with transactionID: \(result.transactionId)")
    }

    func onError(error: TruliooError) {
        print("Verification error code: \(error.code), transactionID: \(error.transactionId), message: \(error.message)")
    }

    func onException(exception: TruliooException) {
        print("Verification exception \(exception.message)")
    }
}
```

------

### 5. Initialize the Trulioo SDK

Call the `Trulioo().initialize` function, and pass in the `workflow` configuration with the obtained short code and `self` as the delegate parameter to initialize the SDK:

```swift
let workflow = TruliooWorkflow(
  shortcode: "sample-short-code",
  locale: "en-US",
  isDemoMode: false,
  enableRegionSelection: true,
  enableDocumentAutoCapture: true
)

// Initialize the SDK with the workflow configuration
Trulioo().initialize(delegate: self, workflow: workflow)
```
------

### 6. Start a verification flow

Finally, call the `Trulioo().launch()` function under the `onInitialized` delegate, and pass in `self` as the delegate parameter to launch the SDK:

```swift
func onInitialized() {
    print("SDK Initialized")
    Trulioo().launch(delegate: self)
}
```

------

### Example ViewController that launches Trulioo

```swift
import UIKit
import TruliooSDK

class YourViewController : UIViewController, TruliooDelegate {
    // Start of Trulioo delegate - Required
    func onInitialized() {
        print("SDK Initialized")
        Trulioo().launch(delegate: self)
    }
    
    func onComplete(result: TruliooSuccess) {
        print("Verification success with transactionID: \(result.transactionId)")
    }

    func onError(error: TruliooError) {
        print("Verification error code: \(error.code), transactionID: \(error.transactionId), message: \(error.message)")
    }

    func onException(exception: TruliooException) {
        print("Verification exception \(exception.message)")
    }
    // End of Trulioo delegate - Required

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let workflow = TruliooWorkflow(shortCode: "sample-short-code")

        // Initialize the SDK with the workflow configuration
        Trulioo().initialize(delegate: self, workflow: workflow)
    }
}
```

------
## Changing Language Locale

Locales are used to set the language of the SDK. This feature is available on version ```2.5.0``` or greater.

The SDK will default to the user's device setting language locale.

If the given language is available but the given locale is not supported, the SDK will set to the given language and the default supported locale.

If the given language locale is not supported, the SDK will default to English (US).

To set language locale, pass in the desired language as a Locale ID (LCID) short string in the `locale` parameter of the TruliooWorkflow object.

```swift
// Find a list of supported locales (LCID) in the table below.
let workflow = TruliooWorkflow(shortCode: "sample-short-code" , locale: "fr-CA")
```

<details>
<summary>Supported Languages & Locales</summary>
<LanguageSupportTable.table />
</details>

------
## Customize Theme

This customizing theme feature is available on version ```2.5.0``` or greater.

To use a custom theme, the SDK provides the TruliooWorkflowTheme object that allows for some customizations.

To set a desired logo for branding, you can pass in the name of a image asset as a string in the `logoAssetName` parameter. The provided logo will be displayed with a fixed height of 32 pixels.

To customize the primary buttons colors, you can set the `primaryButtonColorName` and `primaryButtonTextColorName` parameters with hex color string values.

```swift
let theme = TruliooWorkflowTheme(
    logoAssetName: "Your_Logo",
    primaryButtonColorName: "#172c2d",
    primaryButtonTextColorName: "#a4dbb4")

let workflow = TruliooWorkflow("sample-short-code", theme: theme)
```
---

## Country and Jurisdiction Selection

Each experience can be customized with specific countries and jurisdictions for each document type.

Use the API endpoint `/customer/transactions` to configure all the regions you accept.
Please see 'https://docs.verification.trulioo.com/api/customer/2.5/index.html#tag/Transaction/operation/post-transactions' for more details on the API reference.

Note: If you specify an `empty list` for countries OR specify `all supported` countries, the country list `will not` appear.
Same applies for the list of jurisdictions.

You can `override` the regions selection screens and turn them off from the workflow builder.
Change the parameter 'enableRegionSelection' to false and both screens will be hidden from the user.

```swift
let workflow = TruliooWorkflow(
  shortcode: "sample-short-code",
  locale: "en-US",
  isDemoMode: false,
  enableRegionSelection: true, // Change this to false to override
  enableDocumentAutoCapture: true
)
```


## Handle the verification result

After a session is completed, callback functions are invoked based on the result of the workflow (see above code block for example):

| Callback    | Description                                                                                                                                          |
|:------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|
| `Complete`  | User completed the flow. The `result` object contains the Transaction ID that can be used by the API to gain further insights.                       |
| `Error`     | A user error has occurred. Some images may have already been uploaded. The `result` object contains information about the reason and an `errorCode`. |
| `Exception` | An unexpected exception occurred.                                                                                                                    |

Additionally, you can utilize getSessionResult function call after completing the verification to get the result with the current result Transaction ID:

```swift

Trulioo().getSessionResult() { result in
      print("Transaction status: \(result.status), Transaction ID: \(result.transactionId)")
}
```

The result object should contain the Transaction ID and the verification status which could be one of the following:

| Result     | Description                                                                                                                                                                                                                                           |
|:-----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `Accepted` | This is displayed when the verification result returns a pass result. The identity documentation is verified.                                                                                                                                         |
| `Review`   | The verification results will be flagged for review if there is insufficient information to run the verification test. Checks are run for the validity of the document type, and the quality of the data extracted via Optical Character Recognition. |
| `Declined` | Block fraudsters from your platform with automated rejection criteria. The declined result appears when the verification test returns a fail result and the identity documentation is not verified.                                                   |
| `Error`    | When the verification test is not completed as expected, you’ll see what configuration or authorization issues to troubleshoot. The error message will also identify any missing or improperly captured image data.                                   |


## Handling verification errors

| Code     | Description                                                   |
|:---------|:--------------------------------------------------------------|
| `1010`   | Workflow object does not contain a value for shortcode.       |
| `1020`   | Workflow object contains an invalid shortcode.                |
| `1030`   | The session does not contain a valid access token.            |
| `1320`   | There was a problem on verification session creation.         |
| `1330`   | There was a problem submitting your session for verification. |
| `12020`  | There was a problem retrieving custom configurations.         |
